var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var User = require('../models/user');
var jwt = require('jsonwebtoken');

router.post('/', function (req, res, next) {
    var user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password: bcrypt.hashSync(req.body.password, 10),
        email: req.body.email
    });
    user.save(function(err, result){
        if(err){
            return res.status(500).json({
                title: 'An error has ocurred',
                error: err
            })
        }
        res.status(201).json({
            message: 'Saved message',
            obj: result
        })

    });
});

router.post('/signin', function(req, res, next){
    User.findOne({email: req.body.email }, function(err, user){
        if(err){
            return res.status(500).json({
                title: 'An error has occurred',
                error: err
            });
        }
        if(!user){
            res.status(401).json({
                title: 'User not found',
                error: { message: 'Invalid login credentials' }
            })    
        }
        if(!bcrypt.compareSync(req.body.password, user.password)){
            res.status(401).json({
                title: 'User not found',
                error: { message: 'Invalid login credentials' }
            })
        }
        var token = jwt.sign({ user: user }, 'secret', { expiresIn: 7200 });
        res.status(200).json({
            message: 'Successfully logged in',
            token: token,
            userId: user._id
        })
    });
});

module.exports = router;
